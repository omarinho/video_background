angular.module('starter', ['ionic', 'starter.controllers', 'starter.services','ngIOS9UIWebViewPatch', 'angucomplete', 'ngCordova'])

.run(                     
  function ($ionicPlatform) {
    $ionicPlatform.ready(
      function() {
        if (navigator && navigator.splashscreen) {
          navigator.splashscreen.hide();
        }
      }
    );
  }
)

.run(['$window',
  function($window) {
    function CustomError(message) {
      Error.captureStackTrace(this);
      this.message = message;
      this.name = "SomeCustomError";
    }
    CustomError.prototype = Object.create(Error.prototype);
    $window.CustomError = CustomError;
  }
])

.config(
  function ($stateProvider, $urlRouterProvider) {
    
    $stateProvider
      .state('record_video', {
        url: "/record_video",
        cache: false,
        templateUrl: 'templates/recordvideo.html',
        controller: 'record_videoCtrl'
      })
      .state('view_video', {
        url: "/view_video/:offer_code/:offer_id",
        templateUrl: 'templates/view_video.html',
        controller: 'viewVideoCtrl'
      })
      .state('ready', {
        url: "/ready",
        templateUrl: 'templates/ready.html',
        controller: 'ReadyCtrl'
      })

    $urlRouterProvider.otherwise("/record_video");
  }
)

.config(['$provide', function($provide) {
  $provide.decorator("$exceptionHandler", ['$delegate', function($delegate) {
    return function(exception, cause) {
      $delegate(exception, cause);
      var message = exception.toString();
      var stacktrace = exception.stack.toLocaleString();
      navigator.crashlytics.logException("ERROR: "+message+", stacktrace: "+stacktrace);
    };
  }]);
}])

;