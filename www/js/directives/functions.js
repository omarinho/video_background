var backgroundrecord = {
  timer: null,
  
  initialize: function() {
    this.bindEvents();
  },
  
  bindEvents: function() {
    document.addEventListener('deviceready', this.onDeviceReady, false);
  },

  onDeviceReady: function() {
    backgroundrecord.receivedEvent('deviceready');
  },

  receivedEvent: function(id) {
  },
  
  yay: function(a) {
  },
  
  boo: function(a) {
  },

  start: function(facing) {
    window.Plugin.backgroundvideo.start('StartBtnTest-' + facing, facing, backgroundrecord.yay, backgroundrecord.boo);
  },
    
  stop: function(success, fail) {
    window.Plugin.backgroundvideo.stop(success, fail);
  },
  
  exit: function() {
    navigator.backgroundrecord.exitApp();
  }
};
backgroundrecord.initialize();
