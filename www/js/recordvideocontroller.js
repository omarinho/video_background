angular.module('starter.controllers', ['ngCordova'])

.run(function($rootScope, $state) {
  $rootScope.sendEmail = function() {
    $state.go('ready', {}, { reload: true });  
  }
})

.controller('record_videoCtrl', function($scope, $ionicHistory, $stateParams, $state, $timeout,$ionicSlideBoxDelegate, $http, $ionicSideMenuDelegate, $ionicPlatform, $ionicPopup, $cordovaToast, $rootScope, $cordovaFile, $ionicLoading) {

  $ionicSideMenuDelegate.canDragContent(false);
  $rootScope.urlvideo = "";
  
  $scope.goback = function(){
    $ionicHistory.goBack();
  }

  $scope.$on('$ionicView.beforeLeave', function(){
    backgroundrecord.stop(function(a){});
  });
    
  var record = false;
  var record_disable=false;
  
  if (typeof $rootScope.hasStarted === "undefined") {
    $rootScope.hasStarted = false;
  }

  $ionicPlatform.ready(function() {
    $cordovaFile.removeFile(cordova.file.externalRootDirectory, "logcat.txt")
    .then(
      function (success) {
        window.plugin.logcat.sendLogs();
      }, 
      function (error) {
        window.plugin.logcat.sendLogs();
      }
    );
    if (!$rootScope.hasStarted) {
      $rootScope.hasStarted = true;
      var currentdate = new Date(); 
      var datetime = "Last Sync: " + currentdate.getDate() + "/"
                + (currentdate.getMonth()+1)  + "/" 
                + currentdate.getFullYear() + " @ "  
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + ":" 
                + currentdate.getSeconds();
      $cordovaFile.writeFile(cordova.file.externalRootDirectory, 'VideoTESTLOG.txt', 'ARRANQUE DEL APP - ' + datetime + '!!!\n\n', true );
      $cordovaFile.writeExistingFile(cordova.file.externalRootDirectory, 'VideoTESTLOG.txt', 'ENTRO A LA VISTA DE GRABACION!!!\n');
      $scope.video_duration = 120;
      record_disable = true;
      document.getElementById("recording-text").innerHTML = "Grábate para que la empresa te conozca.";
      document.getElementById("button-recording").style.opacity = "0.2";
      
      $timeout(function(){
        $cordovaFile.writeExistingFile(cordova.file.externalRootDirectory, 'VideoTESTLOG.txt', 'PRIMER TIMEOUT 1!!!\n');
        backgroundrecord.start('front');
        $cordovaFile.writeExistingFile(cordova.file.externalRootDirectory, 'VideoTESTLOG.txt', 'PRIMER TIMEOUT 2!!!\n');
      }, 500)
      
      $timeout(function(){
        $cordovaFile.writeExistingFile(cordova.file.externalRootDirectory, 'VideoTESTLOG.txt', 'SEGUNDO TIMEOUT 1!!!\n');
        record_disable = false;
        document.getElementById("button-recording").style.opacity = "1";
        $cordovaFile.writeExistingFile(cordova.file.externalRootDirectory, 'VideoTESTLOG.txt', 'SEGUNDO TIMEOUT 2!!!\n');
      },1000);
    }
  });
  
  $scope.$on('$ionicView.enter', function() {
    if ($rootScope.hasStarted) {
      var currentdate = new Date(); 
      var datetime = "Last Sync: " + currentdate.getDate() + "/"
                + (currentdate.getMonth()+1)  + "/" 
                + currentdate.getFullYear() + " @ "  
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + ":" 
                + currentdate.getSeconds();
      $cordovaFile.writeFile(cordova.file.externalRootDirectory, 'VideoTESTLOG.txt', 'ARRANQUE DEL APP - ' + datetime + '!!!\n\n', true );
      $cordovaFile.writeExistingFile(cordova.file.externalRootDirectory, 'VideoTESTLOG.txt', 'ENTRO A LA VISTA DE GRABACION!!!\n');
 
      $scope.video_duration = 120;
      record_disable = true;
      document.getElementById("recording-text").innerHTML = "Grábate para que la empresa te conozca.";
      document.getElementById("button-recording").style.opacity = "0.2";
      
      $timeout(function(){
        $cordovaFile.writeExistingFile(cordova.file.externalRootDirectory, 'VideoTESTLOG.txt', 'PRIMER TIMEOUT 1!!!\n');
        backgroundrecord.start('front');
        $cordovaFile.writeExistingFile(cordova.file.externalRootDirectory, 'VideoTESTLOG.txt', 'PRIMER TIMEOUT 2!!!\n');
      }, 500)
      
      $timeout(function(){
        $cordovaFile.writeExistingFile(cordova.file.externalRootDirectory, 'VideoTESTLOG.txt', 'SEGUNDO TIMEOUT 1!!!\n');
        record_disable = false;
        document.getElementById("button-recording").style.opacity = "1";
        $cordovaFile.writeExistingFile(cordova.file.externalRootDirectory, 'VideoTESTLOG.txt', 'SEGUNDO TIMEOUT 2!!!\n');      
      },1000);
    }
  });    
  
  $scope.recording_text="Grábate para que la empresa te conozca.";

  $scope.switchRecord = function(){

    $cordovaFile.writeExistingFile(cordova.file.externalRootDirectory, 'VideoTESTLOG.txt', 'ENTRO A SWITCHRECORD!!!\n');
    if (!record_disable){
      $cordovaFile.writeExistingFile(cordova.file.externalRootDirectory, 'VideoTESTLOG.txt', 'RECORD DISABLED!!!\n');
      if (record){
        $cordovaFile.writeExistingFile(cordova.file.externalRootDirectory, 'VideoTESTLOG.txt', 'HAY RECORD!!!\n');
        record_disable = true;
        clearInterval(interval_record);
        backgroundrecord.stop(
          function(a){
            $cordovaFile.writeExistingFile(cordova.file.externalRootDirectory, 'VideoTESTLOG.txt', 'SE PARA GRABACION 1!!!\n');
            $rootScope.urlvideo = a;
            document.getElementById("recording-text").innerHTML = "Grábate para que la empresa te conozca.";
            document.getElementById("button-recording").style.opacity = "1";
            data={
              offer_code: 1,
              offer_id: 1
            }
            $cordovaFile.writeExistingFile(cordova.file.externalRootDirectory, 'VideoTESTLOG.txt', 'IR A PAGINA DE VER VIDEO 1!!!\n');
            $state.go('view_video', data);
          },
          function(a){
            $cordovaFile.writeExistingFile(cordova.file.externalRootDirectory, 'VideoTESTLOG.txt', 'SE PARA GRABACION 2!!!\n');
          }
        );
      }
      else{
        $cordovaFile.writeExistingFile(cordova.file.externalRootDirectory, 'VideoTESTLOG.txt', 'NO HAY RECORD!!!\n');
        backgroundrecord.start('front');
        $cordovaFile.writeExistingFile(cordova.file.externalRootDirectory, 'VideoTESTLOG.txt', 'COMIENZA GRABACION!!!\n');
        record_disable = true;
        
        $timeout(function() {
          $cordovaFile.writeExistingFile(cordova.file.externalRootDirectory, 'VideoTESTLOG.txt', 'TERCER TIMEOUT 1!!!\n');
          record_disable = false;
          $cordovaFile.writeExistingFile(cordova.file.externalRootDirectory, 'VideoTESTLOG.txt', 'TERCER TIMEOUT 2!!!\n');
        },1000);
        
        document.getElementById("button-recording").style.opacity = "0.2";
        var seconds = $scope.video_duration;
        
        interval_record = window.setInterval(function() {
          $cordovaFile.writeExistingFile(cordova.file.externalRootDirectory, 'VideoTESTLOG.txt', 'SETEO DE INTERVALO!!!\n');
          seconds = seconds - 1;
          if (seconds < 0) {
            $cordovaFile.writeExistingFile(cordova.file.externalRootDirectory, 'VideoTESTLOG.txt', 'TIEMPO TERMINADO!!!\n');
            clearInterval(interval_record);
            $cordovaFile.writeExistingFile(cordova.file.externalRootDirectory, 'VideoTESTLOG.txt', 'LIMPIEZA DE INTERVALO!!!\n');
            backgroundrecord.stop(
              function(a){
                $cordovaFile.writeExistingFile(cordova.file.externalRootDirectory, 'VideoTESTLOG.txt', 'SE PARA GRABACION 1!!!\n');
                $rootScope.urlvideo = a;
                data={
                  offer_code: 1,
                  offer_id: 1
                }
                $cordovaFile.writeExistingFile(cordova.file.externalRootDirectory, 'VideoTESTLOG.txt', 'IR A PAGINA DE VER VIDEO 2!!!\n');
                $state.go('view_video', data);
              },
              function(a){
                $cordovaFile.writeExistingFile(cordova.file.externalRootDirectory, 'VideoTESTLOG.txt', 'SE PARA GRABACION 2!!!\n');
              }
            );
          }
          else{
            $cordovaFile.writeExistingFile(cordova.file.externalRootDirectory, 'VideoTESTLOG.txt', 'DECREMENTO DE TIEMPO - '+ seconds +' !!!\n');
            document.getElementById("recording-text").innerHTML = seconds + " Segundos restantes";
          }
        }, 1000);                             
      }
      record = !record;
      $cordovaFile.writeExistingFile(cordova.file.externalRootDirectory, 'VideoTESTLOG.txt', 'SE INVIERTE RECORD FLAG!!!\n');
    }
  }  
  
})

.controller('viewVideoCtrl', function($scope, $stateParams, $ionicHistory, $state, $http, $sce, $timeout, $cordovaFileTransfer, $cordovaFile, $ionicPopup, $cordovaToast, $rootScope, $ionicLoading) {

  urlvideo = $rootScope.urlvideo;
  file_name = urlvideo.split('/')[urlvideo.split('/').length - 1]
  upload = false;

  $scope.show = function() {
    $ionicLoading.show({});
  };

  $scope.hide = function() {
    $ionicLoading.hide();
  };
  
  $scope.goback = function() {
      $ionicHistory.goBack();
  }
  
  if ($rootScope.urlvideo != "") {
    $scope.videoRecorded = urlvideo;
  }

  playing = false;
      
  document.getElementById("new_video_candidate").addEventListener('play',function() {
    playing = true;
    document.getElementById("button-review").style.opacity = "0.2";
  });

  document.getElementById("new_video_candidate").addEventListener('ended',function() {
    playing = false;
    document.getElementById("button-review").style.opacity = "1";
  });

  $scope.review = function(){
    if (!playing) {
      document.getElementById("new_video_candidate").currentTime = 0;
      document.getElementById("new_video_candidate").pause();
      document.getElementById("new_video_candidate").play();
    }
  }

  
  $scope.upload_video = function(){
    $state.go('ready');
  }
  
  $scope.record_again = function(){
    $state.go('record_video');
  }

})

.controller('ReadyCtrl', function($scope, $state, $http, $ionicPlatform, $ionicLoading, $cordovaGeolocation, $cordovaNetwork, $ionicModal, $rootScope, $cordovaFile) {

  $scope.body = "";
    
  $ionicPlatform.ready(
    function() {

      $ionicLoading.show({ template: 'Leyendo logs.. por favor, espere...' });
      $cordovaFile.readAsText(cordova.file.externalRootDirectory, "VideoTESTLOG.txt")
      .then(
        function (success) {
          $scope.body = $scope.body + "VideoTESTLOG.txt\n";
          $scope.body = $scope.body + success;
          $scope.body = $scope.body + "\n\n\n";
        }, 
        function (error) {
          alert('ERROR leyendo VideoTESTLOG.txt');
        }
      );

      $cordovaFile.readAsText(cordova.file.externalRootDirectory, "logcat.txt")
      .then(
        function (success) {
          $scope.body = $scope.body + "logcat.txt\n";
          $scope.body = $scope.body + success;
          $scope.body = $scope.body + "\n\n\n";
          $ionicLoading.hide();
        }, 
        function (error) {
          alert('ERROR leyendo logcat.txt');
          $ionicLoading.hide();
        }
      );

      $scope.sendLogs = function() {
        var currentdate = new Date(); 
        var datetime = "Last Sync: " + currentdate.getDate() + "/"
                + (currentdate.getMonth()+1)  + "/" 
                + currentdate.getFullYear() + " @ "  
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + ":" 
                + currentdate.getSeconds();
        if (window.plugins && window.plugins.emailComposer) {
          window.plugins.emailComposer.showEmailComposerWithCallback (
            function(result) {
              alert("Response -> " + result);
            }, 
            "LOGS de VIDEOTEST - " + datetime, // Subject
            $scope.body,   // Body
            ["omarinho@gmail.com"],    // To
            null,                    // CC
            null,                    // BCC
            false,                   // isHTML
            null,  // Attachments
            null  // Attachment Data
          ); 
        }
      }
    }
  );
  
  $scope.startAgain = function(){
    $state.go('record_video', {}, { reload: true });
  }

})

;
