package io.iclue.backgroundvideo;

import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.util.DisplayMetrics;

import java.util.List;

class CameraHelper {
    public static Camera.Size getPreviewSize(Camera.Parameters cp) {
        Camera.Size size = cp.getPreferredPreviewSizeForVideo();
        
        if (size == null)
            size = cp.getSupportedPreviewSizes().get(0);
        
        return size;
    }
    
    public static Camera.Size getLowestResolution (Camera.Parameters cp) {
        List<Camera.Size> sl = cp.getSupportedVideoSizes();
        
        if (sl == null)
            sl = cp.getSupportedPictureSizes();

        Camera.Size small = sl.get(0);

        for(Camera.Size s : sl) {
            if ((s.height * s.width) < (small.height * small.width))
                small = s;
        }

        return small;
    }

    public static Camera.Size getOptimalPreviewSize(Camera.Parameters p) {
        List<Camera.Size> sizes = p.getSupportedPreviewSizes();

        DisplayMetrics displaymetrics = new DisplayMetrics();

        int w = displaymetrics.widthPixels;
        int h = (int)(displaymetrics.heightPixels * 0.75);

        final double ASPECT_TOLERANCE = 0.2;
        double targetRatio = (double) w / h;
        if (sizes == null)
            return null;

        Camera.Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;

        int targetHeight = h;

        // Try to find an size match aspect ratio and size
        for (Camera.Size size : sizes) {
            double ratio = (double) size.width / size.height;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE)
                continue;
            if (Math.abs(size.height - targetHeight) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }

        // Cannot find the one match the aspect ratio, ignore the
        // requirement
        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Camera.Size size : sizes) {
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }
        return optimalSize;
    }

    public static boolean sizeSupported (Camera.Parameters cp, CamcorderProfile profile) {
        List<Camera.Size> sl = cp.getSupportedVideoSizes();

        if (sl == null)
            sl = cp.getSupportedPictureSizes();

        for(Camera.Size s : sl) {
            if (profile.videoFrameWidth == s.width && profile.videoFrameHeight == s.height)
                return true;
        }

        return false;
    }

    public static Camera getDefaultCamera(int position) {
        // Find the total number of cameras available
        int  mNumberOfCameras = Camera.getNumberOfCameras();

        // Find the ID of the back-facing ("default") camera
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        for (int i = 0; i < mNumberOfCameras; i++) {
            Camera.getCameraInfo(i, cameraInfo);
            if (cameraInfo.facing == position) {
                return Camera.open(i);
            }
        }

        return null;
    }

    public static int getCameraId(int position) {
        // Find the total number of cameras available
        int  mNumberOfCameras = Camera.getNumberOfCameras();

        // Find the ID of the back-facing ("default") camera
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        for (int i = 0; i < mNumberOfCameras; i++) {
            Camera.getCameraInfo(i, cameraInfo);
            if (cameraInfo.facing == position)
                return i;
        }

        return 0;
    }
}
